from lib.phone import phone
import lib.utils as utils
import time, os, threading, random
from colorama import Fore


class instagram:

    def __init__(s, adb_phone, verbose = True):
        s.app = 'com.instagram.android'
        s.phone = phone(adb_phone, s.app)
        s.phone._open(s.app)
        s.state = 'started'
        s.verbose = verbose

        threading.Thread(target=s.statespy).start()
    
    def statespy(s):
        while True:
            screen = s.phone.screenshot()
            '''
            if utils.match_template(screen,'data/templates/instagram/bar_heart.png') is not None:
                s.state = 'news'
                os.remove(screen)
            elif utils.match_template(screen,'data/templates/instagram/bar_home.png') is not None:
                s.state = 'home'
                os.remove(screen)
            elif utils.match_template(screen,'data/templates/instagram/bar_profile.png') is not None:
                s.state = 'profile'
                os.remove(screen)
            elif utils.match_template(screen,'data/templates/instagram/barsearch.png') is not None:
                s.state = 'search'
                os.remove(screen)
            elif utils.match_template(screen,'data/templates/instagram/login.png') is not None:
                s.state = 'login'
                os.remove(screen)
            elif utils.match_template(screen,'data/templates/instagram/upload_icon.png') is not None:
                s.state = 'uploading'
                os.remove(screen)
            elif utils.match_template(screen, 'data/templates/instagram/unfollow.png') is not None:

                s.phone.tap(120, 550, 2)
                print('> unfollow dialog')
            elif utils.match_template(screen,'data/templates/instagram/hashtag.png') is not None:
                print(utils.ocr(screen))
            else:
                s.state = 'unknown'
           
            time.sleep(1)
            '''


    def find_and_tap(s, image, sleep):
        screen = s.phone.screenshot()
        x, y = utils.match_template(screen, image)
        s.phone.tap(x, y, sleep)
        os.remove(screen)

    def login(s,username,passwd):
        time.sleep(3)
        if s.state == 'login': 

            # login button
            s.phone.tap(160, 326, 1)

            # select username
            s.phone.tap(150, 197, 1)
            
            # go home
            s.phone.key('home', 0)
            
            # delete whatever instagram has autofilled
            for i in range(16):
                s.phone.key('del', 0)

            # input username
            s.phone.text(username, 1)

            # select passwd textbox 
            s.phone.tap(150, 150, 1)
            
            # input password
            s.phone.text(passwd, 1)

            # press login button 
            s.phone.tap(160, 220, 1)

        else:
            print('already logged in')

    def logout(s):
        time.sleep(3)
        # move to profile panel
        s.phone.tap(289, 460, 0)
  
        # open drawer
        s.phone.tap(295, 45, 1)
        
        # open settings
        s.phone.tap(234, 463, 0)

        # scroll all the way down
        s.phone.swipe(300, 300, 0, 0, 100, 1)

        # press logout button
        s.phone.tap(47, 381, 1)

        # confirm logout
        s.phone.tap(243, 304, 1)
    
    def goto_search(s):
        time.sleep(1)
        try:
            s.find_and_tap('data/templates/instagram/searchffs.png', 1)
        except TypeError:
            if(s.verbose): print(Fore.RED + "[{}][{}] lost track badly , restarting app ".format(name, user))
            s.phone._close(s.app)
            s.phone._open(s.app)
            time.sleep(30)
            s.find_and_tap('data/templates/instagram/bar_search_unselected.png', 1)


      

    def search_for_user(s, user):
        name = s.phone.name

        # find search bar
        # input 

        #
        error = False
            # delete whatever instagram has autofilled

        if(s.verbose): print(Fore.YELLOW + "[{}][{}] input username".format(name, user))
        s.phone.text(user, 3)
     
        # select first item
        if(s.verbose): print(Fore.YELLOW + "[{}][{}] tap first item".format(name, user))
        s.phone.tap(180, 155, 3)


        try:
        # follow 
            if(s.verbose): print(Fore.YELLOW + "[{}][{}] press follow button".format(name, user))
            s.find_and_tap('data/templates/instagram/follow.png', 1)
        except TypeError:
            error = True
            
        try:
            if(s.verbose): print(Fore.YELLOW + "[{}][{}] press back button".format(name, user))
            s.find_and_tap('data/templates/instagram/arrow_back.png', 1)
    
        except TypeError:
            s.phone._close(s.app)
            s.phone._open(s.app)
            time.sleep(30)
            s.find_and_tap('data/templates/instagram/bar_search_unselected.png', 1)
            return


        try:
            s.find_and_tap('data/templates/instagram/clear.png', 1)
            if(s.verbose): print(Fore.YELLOW + "[{}][{}] press clear button".format(name, user))

        except TypeError:

            s.phone.key('home', 0)

            for i in range(50):
                s.phone.key('del',0)
                
            if(s.verbose): print(Fore.RED + "[{}][{}] error finding clear button, deleting manually ".format(name, user))


    def search_for_hashtag(s, hashtag):
        time.sleep(3)
        try:
            
    
            s.find_and_tap('data/templates/search_bar.png', 1)
            s.find_and_tap('data/templates/tags.png', 1)
            s.find_and_tap('data/templates/search_hashtags.png', 3)
            print(hashtag)
            s.phone.text(hashtag, 2)
            
            # select first hashtag in results
            s.phone.tap(180, 155, 2)

            # select first ost
            s.phone.tap(70, 600, 2)
        except TypeError:
            print('failed searching for hashtag')
            time.sleep(1)
            s.go_home()
            s.search_for_hashtag(hashtag)
      
    def go_home(s):
        print('> returning to home')
        s.phone.tap(23,40,1)
        s.phone.tap(23,40,1)
        s.phone.tap(40,880,1)
        


        time.sleep(3)
    
    def follow(s,DEPTH):
        followed = 0
        for i in range(25):
            # follow
            try:
                s.phone.swipe(200, 800, 200, 300, 400, 2)
                s.find_and_tap('data/templates/follow.png', 1)
                followed += 1
                print('> followed acounts {}'.format(followed))
            except TypeError:
            # goto_next
                print('> scroll down')
                s.phone.swipe(200, 800, 200, 300, 400, 3)
                s.phone.tap(random.randint(0,400),random.randint(0,400),1)
             
    
        return followed





    

