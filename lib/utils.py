
def match_template(source, _template):
    import cv2, math
    import numpy as np

    i = 0
    x = 0
    y = 0

    img_rgb = cv2.imread(source)
    img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)
    template = cv2.imread(_template ,0)
    w, h = template.shape[::-1]

    res = cv2.matchTemplate(img_gray,template,cv2.TM_CCOEFF_NORMED)
    threshold = 0.8
    loc = np.where( res >= threshold)

    for pt in zip(*loc[::-1]):
        i += 1
        x += pt[0]
        y += pt[1]
    
    if i is 0:
        return -1
    else:
        x = math.floor(x / i + w / 2)
        y = math.floor(y / i + h / 2)

        return x, y

def ocr(source):
    import cv2, pytesseract
    return pytesseract.image_to_string(cv2.imread(source))

def load_hashtags(source):
    tags = []

    import json
    json_data = open('hashtags/meme.json').read()
    data = json.loads(json_data)

    for item in data['posts']:
        for tag in item['tags']:
            tags.append(tag)

    print(len(tags))
    return tags

