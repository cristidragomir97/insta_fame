SCREENS_PATH = '/home/uc0de/Code/AndroidBot/data/screens'

from adb.client import Client as AdbClient
from subprocess import Popen, PIPE
import signal, time, datetime
# Default is "127.0.0.1" and 5037

class phone:

    def __init__(self,name,app):
        client = AdbClient(host="127.0.0.1", port=5037)
        self.device = client.device(name)
        self.name = name
        self.app = app

    def install_apk(self,path):
        self.device.install(path)

    def is_running(self,app):
        return len(self.device.shell('pidof {}'.format(app))) > 0

    def _open(self, app):
        self.device.shell('monkey -p {} -c android.intent.category.LAUNCHER 1'.format(app))
        print(self.is_running(app))

    def _close(self, app):
        self.device.shell('am force-stop {}'.format(app))
        
    def key(self, key, sleep):
        if self.is_running(self.app):
            if key == "home":
                self.device.shell('input keyevent 123')
                time.sleep(sleep)
            elif key == "del":
                self.device.shell('input keyevent 67')
                time.sleep(sleep)
            elif key == "enter":
                self.device.shell('input keyevent 66')
                time.sleep(sleep)
            else:
                print('keycode not implemented')
        else:
            print('app has crashed, not doing shit')
       
    
    def text(self, data, sleep):
        if self.is_running(self.app):
            self.device.shell("input text {}".format(data))
            time.sleep(sleep)
        else:
            print('app has crashed, not doing shit')

    def tap(self, x, y, sleep):
        if self.is_running(self.app):
            self.device.shell("input tap {} {}".format(x, y))
            time.sleep(sleep)
        else:
            print('app has crashed, not doing shit')

    def swipe(self, x1, y1, x2, y2, ms, sleep):
        if self.is_running(self.app):
            self.device.shell('input swipe {} {} {} {} {}'.format(x1,y1,x2,y2,ms))
            time.sleep(sleep)
        else:
            print('app has crashed, not doing shit')

    def screenshot(self):
        now = str(datetime.datetime.utcnow()).replace(" ", "_")
        _file = "_{}_{}.png".format(self.name, now)
        _outfile = "{}/{}".format(SCREENS_PATH,_file)
        self.device.shell("screencap -p /sdcard/{}".format(_file))
        self.device.pull("/sdcard/{}".format(_file), _outfile)
        self.device.shell("rm -f /sdcard{}".format(_file))
        return _outfile



 


