import time, serial, re, datetime, json
import smspdu.easy as decoder

# STATIC
def send_command(command, ser, timeout):
    command = command + "\r\n"
    ser.write(command.encode())
    time.sleep(timeout)
    read_me = ser.read(1024).decode('unicode_escape')
    return read_me

def init(PORT, verbose=False):
    ser = serial.Serial( str(PORT), 115200, timeout=1)

    # Echo disabled
    response = send_command("ATE0", ser, 0)
    if verbose: print("[-]COM{}@set_no_echo: {}".format(PORT, response))

    # Get manufacturer info
    response = send_command("AT+GMI", ser, 0)
    if verbose: print("[-]COM{}@get_manufacturer_info: {}".format(PORT, response))

    # Set text mode
    response = send_command("AT+GMM", ser, 0)
    response += send_command("AT+GSN", ser, 0)
    response += send_command("AT+CMGF=?", ser, 0)
    if verbose: print("[-]COM{}@set_text_mode: {}".format(PORT, response))

    # Set encoding to ISO-8859-1
    response = send_command("AT+CSCS=?", ser, 0)
    response += send_command("AT+GCAP", ser, 0)
    response += send_command("AT+CSCS=\"8859-1\"", ser, 0)
    if verbose: print("[-]COM{}@set_iso_mode: {}".format(PORT, response))

    # Store messages to SIM card
    response = send_command("AT+CMGF=0", ser, 0)
    response += send_command("AT+CPMS=?", ser, 0)
    response += send_command("AT + CPMS = \"SM\"", ser, 0)
    if verbose: print("[-]COM{}@set_storage_sim: {}".format(PORT, response))

    ser.close()


def number(port, verbose=False):
    print(port)
    try:
        ser = serial.Serial( str(port), 115200, timeout=1)
        response = send_command("AT+CNUM", ser, 0)
        ser.close()

        phonenumber = re.search("\d{11}", response).group(0)
        if verbose: print("[-]COM{}@get_number_cnum: {}".format(port, phonenumber))
        return port, phonenumber

    except Exception as e:
        print("[-] Error: {}".format(e))
        return port, -1


def sms(port, verbose=True):

    sms_list = []
    ser = serial.Serial( str(port), 115200, timeout=0)

    response = send_command("AT+CPMS=\"SM\"", ser, 1) + \
               send_command("AT+CMGL=1", ser, 1) + \
               send_command("AT+CMGL=0", ser, 1)


    ser.close()

    try:
        pduList = re.findall('\w{9,}', response)
        pduList = pduList[::-1]

        for pdu in pduList:
            sms_list.append(str(decoder.easy_sms(pdu)))

        return sms_list
    except Exception as e:
        print(e)
        return


if __name__ == '__main__':
    
    for i in range(0,31):
        PORT = '/dev/ttyXRUSB{}'.format(i)
        _port, _number = number(PORT)
        _sms = sms(PORT)

        print(json.dumps({
            'port': _port,
            'number': _number,
            'sms':_sms
        }))
        #except Exception as e:
        #   return
            #print(PORT,e)

    