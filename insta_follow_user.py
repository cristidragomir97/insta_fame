from lib.instagram import instagram
import lib.utils, time,  threading, random, sys
from colorama import Fore

users = list(open('data/files/users.txt').read().replace(' ','').split('\n'))

def do_the_thing(insta_instance, user):
        insta_instance.search_for_user(user)
        with open('data/files/followed.txt','a') as f:
                f.write('{}\n'.format(user))


DEPRH = 100 

def _instance( device):
    insta = instagram(device)
  
    insta.goto_search()
    
    while True:
        user = users[random.randint(0,len(users))]
        if user not in list(open('data/files/followed.txt').read().replace(' ','').split('\n')):
               do_the_thing(insta,user)
        else:
               user = users[random.randint(0,len(users))]
               do_the_thing(insta,user)
        
        _time = random.randint(15,300)
        print(Fore.GREEN + '[{}] sleeping for {} sec'.format(device,_time))

        time.sleep(_time)
        


if __name__ == '__main__': 
      _instance(sys.argv[1])